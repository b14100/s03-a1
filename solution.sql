USE blog_db;

-- Creating rows for users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", 20210101010000);
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", 20210101020000);
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", 20210101030000);
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", 20210101040000);
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", 20210101050000);

-- Creating rows for posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", 20210102010000);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", 20210102020000);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", 20210102030000);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", 20210102040000);

-- Get all post with an author id 1
SELECT * FROM posts WHERE author_id = 1;

-- Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post by using the record's ID
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete a user
DELETE FROM users WHERE email = "johndoe@gmail.com";